<?php
class PageBuilder
{

    const pages = [
        '/' => 'Home',
        '/projects/' => 'Projects',
        '/skills/' => 'Skills'
    ];

    public static function getHead()
    {
        $pageTitle = self::pages[$_SERVER['REQUEST_URI']];

        return <<<HTML
        <head>
            <link rel="stylesheet" href="/style.css">
            <link rel="icon" type="image/png" href="/favicon.png" />
            <title>Thomas Næsje | $pageTitle</title>
        </head>
HTML;
    }

    public static function getHeader()
    {


        $htmlBefore = <<<'SOMETHING'
        <div class="header">
            <div class="header-inner">
                <a href="/" class="header-link">
                    <img src="/logo_medium.png">
                    <h1>Thomas Næsje</h1>
                </a>
                <ul class="header-navigation">
SOMETHING;


        $htmlAfter = <<<'SOMETHING'
                </ul>
            </div>
        </div>
SOMETHING;


        $links = [];
        $pages = self::pages;
        foreach ($pages as $path => $pageName) {
            if ($_SERVER['REQUEST_URI'] === $path) {
                $links[] = '<li class="active"><a href="' . $path . '">' . $pageName . '</a></li>';
            } else {
                $links[] = '<li><a href="' . $path . '">' . $pageName . '</a></li>';
            }
        }

        return $htmlBefore . implode('', $links) . $htmlAfter;
    }
}
