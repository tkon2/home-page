<?php

include '../src/PageBuilder.php';

$items = [];

$apps = array_values(array_diff(scandir('../apps'), array('..', '.')));

foreach ($apps as $app) {
    $image_url = '/apps/' . $app . '/feature.png';
    $app_url = '/apps/' . $app . '/';
    $blurb_path = '../apps/' . $app . '/blurb.txt';
    $blurb_content = file_get_contents($blurb_path);
    $title = str_replace('_', ' ', ucwords($app, '_'));

    // Set image to default if none is found
    if (!file_exists('..' . $image_url)) {
        $image_url = '/logo_big.png';
    }

    $item = '<a class="site-link" href="' . $app_url . '">';
    $item .=    '<div class="site-card">';
    $item .=        '<div class="site-image">';
    $item .=            '<img src="' . $image_url . '">';
    $item .=        '</div>';
    $item .=        '<div class="site-content">';
    $item .=            '<h2>' . $title . '</h2>';
    $item .=            '<p>' . ($blurb_content ?: '') . '</p>';
    $item .=        '</div>';
    $item .=    '</div>';
    $item .= '</a>';

    $items[] = $item;
}

?>

<html>

<?= PageBuilder::getHead() ?>

<body>

    <?= PageBuilder::getHeader() ?>
    <div class="main-content">
        <div class="page-header-text">
            <h1>Projects</h1>
        </div>
        <?= implode($items) ?>
    </div>
</body>

</html>