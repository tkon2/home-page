<?php

include '../src/PageBuilder.php';

?>

<html>

<?= PageBuilder::getHead() ?>

<body>

    <?= PageBuilder::getHeader() ?>
    <div class="main-content">
        <div class="page-header-text">
            <h1>Skills</h1>
        </div>
        <div>
            <div class="text-block">
                <h2>Introduction</h2>
                <p>
                    This page serves as a place where I put things I've learned or
                    decided to look into. The idea is that it will be a continuously
                    updated wall of achievements. The posts here are long-form
                    write ups of my progress in a certain topic, but may not itself
                    stay on topic.
                </p>
            </div>
            <div class="text-block">
                <h2>Linux</h2>
                <p>
                    I think it's only right to start with linux. It has very quickly
                    become my favourite OS, where I was previously a fan of Windows.
                    Linux has become my stable ground where I make all my other
                    discoveries and learning efforts.
                    By its very nature I think it challenges the ones who want to
                    be challenged to explore more and more of its inner workings. In
                    using it daily both privately and professionally I have learned
                    many things about linux, but also about the many tools in the
                    ecosystem. For instance I have gained experience with vim, my
                    new favourite command line editor. I'm also close friends with
                    the likes of ls, cat, cs, mkdir, rm, top (htop), apt, find, and
                    many, many more. In setting up an environment
                    for myself on linux I have learned many things about the
                    many dotfiles and other config or settings file available. The
                    ability to customize your experience with linux is unmatched.
                    Every component can be replaced just as easily as you can switch
                    web browsers on other systems. That includes your image viewer,
                    your bluetooth manager, your file explorer, or even your start
                    menu.
                    linux feels like it's made exactly for people like me - the ones
                    who are addicted to learning and exploring the possibilities.
                </p>
            </div>
            <div class="text-block">
                <h2>Window Managers</h2>
                <p>
                    Another linux-related thing I have delved into is what's called
                    window managers. Basically these are very simple graphical
                    interfaces that instead of allowing you to move windows around
                    freely, locks them into a grid. These WMs normally don't
                    allow overlapping of windows, and so the benefit is that you
                    can always see all of your windows. This can help the user
                    stay vigilant about closing unused windows and finishing tasks
                    before moving on. The one I used is called
                    <a href="https://i3wm.org">i3</a> and this worked very well
                    for me for a while. Recently I have gone back to the Cinnamon
                    desktop however, as I currently find myself with a lot of
                    multitasking to do, and even with virtual desktops, WMs can't
                    compete with layering windows. Another issue is that some
                    programs don't work very well at certain window sizes, and so
                    it can be hard to use them effectively. It can also be hard
                    to collaborate as WM settings are usually very personalized
                    and not that many people use them in the first place.
                </p>
            </div>
            <div class="text-block">
                <h2>JavaScript</h2>
                <p>
                    I have done many small and large projects with JavaScript. From
                    react and react-native to the many basic JS apps on this site,
                    I have become very familiar with the language. I started
                    coding in JavaScript just as ES6 was becoming popular, so most
                    of my code uses the new syntax. JavaScript is great because
                    every computer comes with a runtime environment, so anyone
                    can pick up a keyboard and start coding right away. Although I
                    speak about it in a later post,
                    <a href="https://p5js.org/">p5.js</a> has a great deal to do
                    with why I enjoy JavaScript so much in particular. I would also
                    like to highlight Daniel Shiffman, or
                    <a href="https://www.youtube.com/user/shiffman">The Coding Train</a>
                    on youtube. He makes great videos about coding and the p5
                    library.
                </p>
            </div>
            <div class="text-block">
                <h2>Vector Images</h2>
                <p>
                    When I was creating this site, I decided I would need some art
                    and a logo for it. I have previously done pixel art, but I
                    wanted to try something new. I decided on vector images, as
                    it is a very different way of drawing, but one which has many
                    different benefits. I downloaded
                    <a href="https://inkscape.org/">inkscape</a> and started drawing,
                    and finally settled on the hexagonal icon and image you see on
                    the page today. I will probably do more vector art in the future,
                    as the benefits for future proofing are great, and it is a more
                    fun way to draw. Pixel art will probably always be my favourite
                    though.
                </p>
            </div>
            <div class="text-block">
                <h2>Automatic Deployment</h2>
                <p>
                    I have worked with bitbucket pipelines and gitlab CI/CD to
                    have seamless and easy deployment of apps. These technologies
                    are great, particularly for keeping a server up to date with
                    any code updates from anywhere. Especially for this site,
                    as I am liable to make changes from anywhere in the world,
                    not having to keep an SSH key on me at all times is such a
                    relief. It makes the whole process super simple, and I would
                    highly recommend looking into it. Personally I think gitlab
                    offers the better service, particularly since it offers private
                    repositories for free.
                </p>
            </div>
            <div class="text-block">
                <h2>3D Modelling</h2>
                <p>
                    For my gallery tour project, which was a project for university,
                    I wanted to use
                    <a href="https://www.blender.org/">blender</a> to create the
                    3D environment. The
                    JavaScript library used in this example is not p5, but rather
                    <a href="https://threejs.org/">three.js</a>, which would make
                    game designers or 3D modeller feel more at home. The main
                    reason three.js was used was because it allowed for a model
                    import directly from blender. Blender is an amazing piece of
                    software. It provides facilities for 3D modelling, animation,
                    video editing, and even sound editing. I only scratched the
                    surface in creating the room model, and I really should explore
                    it more in the future.
                </p>
            </div>
            <div class="text-block">
                <h2>The p5 Library</h2>
                <p>
                    This library is the reason I code for fun. It provides a great
                    interface to quickly create something beautiful. For instance,
                    look at this
                    <a href="https://p5js.org/examples/simulate-snowflakes.html">
                        simulation of snowflakes
                    </a>
                    in the p5.js examples. The library offers both 2D and 3D
                    HTML5 canvas rendering, and in fact I took advantage of both
                    of these modes with my dissertation project. The library
                    includes many helpful functions to help create great things,
                    such as noise functions. One commonly used noise is perlin
                    noise. The special thing about this noise is that it's smooth.
                    That means you could use it to generate something like terrain
                    where you don't want sharp changes, as that would look unnatural.
                    Other functions included in the library are things like min, max,
                    and mapping functions (the latter being particularly useful),
                    curves, pre-made shapes, values like π, π/2, and tau, and even
                    DOM interaction. DOM manipulation is handled through a separate
                    file from the main library, so you can choose on a per-project
                    basis whether to include it. There is also an audio library
                    available, which is a lot of fun to play with. Please go to the
                    <a href="https://p5js.org/">p5.js website</a> where you can even
                    test it online before downloading anything with their online editor.
                </p>
            </div>
            <div class="text-block">
                <h2>Electronics</h2>
                <p>
                    As a part of my studies in North Norway, we launched a sounding
                    rocket with student made electronics on board. My team and I
                    were to create a MUX board, so more students could get telemetry
                    from their boards. This gave me the opportunity to go through
                    the whole process. From component selection to planning out a
                    board layout, to etching our own board. Of course these days
                    a PCB from China is quick and cheap, and even UK made boards
                    are available at competitive prices and very short delivery
                    timeframes. Still, the skills of selecting components, laying
                    out the board, and soldering on the components are always useful
                    and will stay with me.
                </p>
            </div>
            <div class="text-block">
                <h2>Arduino</h2>
                <p>
                    One of my first languages was actually arduino. I have done many
                    projects with arduinos, for instance several computer input
                    methods, including a set of typing gloves which upon making
                    contact between two fingers (for instance your left index finger
                    and your right middle finger) would type a letter or a number.
                    The gloves unfortunately didn't fit me very well, and nobody but
                    me could be bothered to learn how to use them, so they're shelved
                    for now. Another input method I created was a kind of remote
                    control for my computer. A small box had 5 or 6 buttons on it
                    to do simple things like play/pause a playback, volume control,
                    or track skip. Arduinos can mimic input devices, even mouse
                    inputs can be controlled. This means there is a whole world of
                    custom games with custom controllers that can be opened up. I
                    have a few projects in mind, but you'll have to wait and see.
                </p>
            </div>
        </div>
    </div>
</body>

</html>