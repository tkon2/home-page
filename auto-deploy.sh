#! /bin/bash

branch='master'

git checkout $branch

if git pull --ff-only
then
    echo "Successfully pulled changes"
else
    date=$(date +"%Y-%m-%d_%H-%M")

    git checkout -b server_changes_$date
    git add .
    git commit -m "Server changes at $date"
    git checkout $branch

    git pull --ff-only
fi

if command -v sass &> /dev/null
then
    sass style.scss:style.css
else
    echo "sass command not found"
fi
