<?php

include './src/PageBuilder.php';

?>

<html>

<?= PageBuilder::getHead() ?>

<body>

    <?= PageBuilder::getHeader() ?>
    <div class="main-content">
        <div class="page-header-text">
            <h1>Home</h1>
        </div>
        <div>
            <div class="text-block">
                <h2>LAMP Stack Talk at BLUG</h2>
                <p>
                    This is a talk I did for the
                    <a href="https://www.birminghamlug.org.uk/">Birmingham LUG</a>
                    about the LAMP stack. It is about an hour long and covers the basics
                    of MySQL, PHP, and Apache setup on Linux, and showcases a simple site
                    built with these technologies. The talk doesn't go into depth about
                    Linux setup as the audience is already very familiar with that part
                    of the web stack. The talk was a great opportunity to learn a bit
                    more about the LAMP stack, especially as I believe in the saying 
                    "Teaching is the best way to learn".
                </p>
                <div class="centered-content">
                    <iframe 
                        width="560" 
                        height="315" 
                        src="https://www.youtube.com/embed/fqzygieW0eI" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen>
                    </iframe>
                </div>
            </div>

            <div class="text-block">
                <h2>Hello, world!</h2>
                <p>
                    I'm Thomas, and this is my website. I built it to act as a place to
                    put smaller projects I work on. The site itself is simple PHP, and 
                    mostly static to provide better load times. Personally I greatly 
                    value this, and especially with a good pipeline setup is just as 
                    simple as any database-based setup. As an added bonus you get to use
                    your favourite development environment! I think working on things
                    like this which are outside the norm really helps pick appart what
                    actually matters, and more than that, why the frameworks exist. 
                </p>
                <p>
                    On this site you will find many projects that I've done in my
                    spare time. Most of these are built using the
                    <a href="https://p5js.org/">p5.js library</a>, which makes working
                    with HTML5 canvases very fun and simple. Have a look at the 
                    <a href="/projects/">projects page</a> to see the projects I have
                    worked on in the past. You will find all kinds of things there,
                    such as games, visualisations, and code generators.
                </p>
                <p>
                    Another passion of mine is Linux. I enjoy learning about it for the
                    same reason I enjoy learning about web technologies - there are just
                    so many layers to unfold and learn about. With Linux the learning
                    process is so simple, instead of using a graphical interface you can
                    simply do the same things from the command line. It is amazing the
                    things you can do with a basic understanding of bash and some time.
                </p>
                <p>
                    I recently put more effort into understanding Linux as a part of a
                    whole. In a sense, an operating system is the base for all software,
                    with the exception of microcontrollers and the like. Especially within 
                    web, where it's part of the famous LAMP stack. I find Linux is by far the
                    most interesting operating system we have, being free and <i>free</i>.
                    The fact that a free operating system has become the industry standard
                    is great, and a very positive thing. Being specifically named in the 
                    LAMP stack as well makes a lot of sense considering how the stack is 
                    configured. I did a talk for a
                    <a href="https://www.birminghamlug.org.uk/">local LUG</a>
                    (Linux User Group) about the LAMP stack. It gave me an 
                    interesting insight into how all these components work together.
                    The recording of the talk
                    is available on
                    <a href="https://www.youtube.com/watch?v=fqzygieW0eI">YouTube</a>.
                </p>
            </div>
        </div>
    </div>
</body>

</html>